class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name,:email,:password])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name,:email,:password,:password_confirmation,:current_password])
  end

  private

    def after_sign_in_path_for(resource)
       if current_user.has_role? :admin
          manage_user_path(:id => current_user.id)
       else
          user_path(:id => current_user.id) 
       end	   
    end

    def after_sign_up_path_for(resource)
       user_path(:id => current_user.id)
    end
end
