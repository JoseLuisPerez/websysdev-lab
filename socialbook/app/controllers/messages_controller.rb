class MessagesController < ApplicationController
  before_action :set_message, only: [:edit, :update, :destroy]

  # GET /messages
  # GET /messages.json
  def index
    @messages = Message.all
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
    if params[:id] == current_user.id.to_s
       @messages = current_user.messages
    else
       @messages = Message.where(user_id: [params[:id]]) 
    end
  end

  # GET /messages/new
  def new
    if current_user
      @message = Message.new
    else
      redirect_to new_user_session_path, :alert => "You have to sign in in order to post a message!"
    end
  end

  # GET /messages/1/edit
  def edit
  end

  # POST /messages
  # POST /messages.json
  def create
    @message = Message.new(message_params)
    @message.user = current_user 
	@message.state = params[:option]
    respond_to do |format|
      if @message.save
        format.html { redirect_to new_message_path, notice: 'Message was successfully created.' }
        format.json { render action: 'show', status: :created, location: @message }
      else
        format.html { redirect_to new_message_path, alert: 'Message was not created succesfully' }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /messages/1
  # PATCH/PUT /messages/1.json
  def update
    respond_to do |format|
	  @message.state = params[:option]
      if @message.update(message_params)
        if current_user.has_role? :admin
          format.html { redirect_to message_path(:id => @message.user_id), notice: 'Message was successfully updated.' }
          format.json { render :show, status: :ok, location: @message }
        else
          format.html { redirect_to message_path(:id => current_user.id), notice: 'Message was successfully updated.' }
          format.json { render :show, status: :ok, location: @message }
        end
      else
        format.html { redirect_to edit_message_path, alert: 'Message was not updated succesfully' }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    
    respond_to do |format|
      if current_user.has_role? :admin
		if @message.destroy
        	format.html { redirect_to message_path(:id => @message.user_id), notice: 'Message was successfully destroyed.' }
        	format.json { head :no_content }
		else
			format.html { redirect_to message_path(:id => @message.user_id), alert: 'Message was not destroyed succesfully' }
        	format.json { head :no_content }
		end
      else
		if @message.destroy
       		format.html { redirect_to message_path(:id => current_user.id), notice: 'Message was successfully destroyed.' }
        	format.json { head :no_content }
		else
			format.html { redirect_to message_path(:id => current_user.id), alert: 'Message was not destroyed succesfully' }
        	format.json { head :no_content }
		end
      end
    end
  end

  def upvote 
    @message = Message.find(params[:id])
    if @message.upvote_by current_user
		flash[:notice] = "Message was successfuly upvoted"
	else
		flash[:alert] = "Message was not succesfully upvoted"
	end
    redirect_to :back
  end  

  def downvote
    @message = Message.find(params[:id])
    if @message.downvote_by current_user
	flash[:notice] = "Message was successfuly downvoted"
	else
		flash[:alert] = "Message was not succesfully downvoted"
	end
    redirect_to :back
  end

  def votes
    @messages = current_user.find_voted_items
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
        params.require(:message).permit(:text, :user_id, :image, :remove_image)
    end
end
