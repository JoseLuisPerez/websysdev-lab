class MyDeviseController < Devise::RegistrationsController
    
    def destroy
	  admin =  User.find(Socialbook::Application.config.id).has_role? :admin
      resource.destroy

      if admin
		@user = User.find(Socialbook::Application.config.id)
		sign_in(@user)
		flash[:notice] = "User was successfuly removed"
        redirect_to manage_user_path(:id => @user.id)
      else
        flash[:notice] = "Bye!! Your account has been deleted successfully"
		redirect_to :root
      end
    end

    protected

    def after_update_path_for(resource)
      @user = User.find(Socialbook::Application.config.id)
      sign_in(@user)
      if @user.has_role? :admin
        manage_user_path(:id => @user.id)
      else
        user_path(:id => @user.id)
      end
    end
	
	def update_resource(resource, params)
	  resource.update_without_password(params)
	end


end 
