class UsersController < ApplicationController
  
  def index
	dontShow = []
	dontShow.push(current_user.id)
	(User.all).each { |u| if u.has_role? :admin then dontShow.push(u) end }
	
    @users = User.where.not(id: dontShow) 
  end

  def show
    Socialbook::Application.config.id = current_user.id
  end

  def edit
	if current_user.has_role? :admin
	  Socialbook::Application.config.id = current_user.id
    end
    @user = User.find(params[:id])
    sign_in(@user)    
    redirect_to edit_user_registration_path
  end

  def friendships
    @u = User.find(params[:id])
    @friends = User.find(params[:id]).friends
    @requested = User.find(params[:id]).requested_friends 
    @pending = User.find(params[:id]).pending_friends    
  end

  def remove_friend
    ids = params[:id].split(",")
    if User.find(ids[0]).remove_friend(User.find(ids[1]))
		flash[:notice] = "Friend was successfully removed"
	else
		flash[:alert] = "Friend was not succesfully removed"
	end
    redirect_to :back
  end

  def friend_request
    if current_user.friend_request(User.find(params[:id]))
		flash[:notice] = "Request was successfully sent"
	else
		flash[:alert] = "Request was not succesfully sent"
	end
    redirect_to :back
  end

  def accept_request
    ids = params[:id].split(",")
    if User.find(ids[0]).accept_request(User.find(ids[1]))
		flash[:notice] = "Request was successfully accepted"
	else
		flash[:alert] = "Request was not succesfully accepted"
	end
    redirect_to :back
  end

  def decline_request
    ids = params[:id].split(",")
    if User.find(ids[0]).decline_request(User.find(ids[1]))
		flash[:notice] = "Request was successfully declined"
	else
		flash[:alert] = "Request was not succesfully declined"
	end
    redirect_to :back
  end

  def manage
	if current_user.has_role? :admin
	  Socialbook::Application.config.id = current_user.id
    end
	sign_in(User.find(Socialbook::Application.config.id))  
    @users = User.where.not(id: [Socialbook::Application.config.id])       
  end



end
