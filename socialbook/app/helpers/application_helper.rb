module ApplicationHelper
  def flash_class(level)
    case level.to_sym
    when :notice then "information"
    when :alert then "alert"
    end
  end
end

