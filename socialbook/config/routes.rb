Rails.application.routes.draw do

  
  get 'welcome/index'  

  devise_for :users, 
           :path => '', 
           :path_names => { :sign_in => 'sign_in', 
                            :sign_up => 'sign_up', 
                            :sign_out => 'sign_out', 
                            :password => 'secret', 
                            :confirmation => 'verification' },
           :controllers => { :registrations => "my_devise" }

 resources :users

 resources :messages do 
   member do
     put "like", to: "messages#upvote"
     put "dislike", to: "messages#downvote"
     get "votes", to: "messages#votes"
   end
end

resources :users do 
   member do
     get "friendships", to: "users#friendships" 
     delete "remove_friend", to: "users#remove_friend" 
     put "friend_request", to: "users#friend_request" 
     put "accept_request", to: "users#accept_request"
     put "decline_request", to: "users#decline_request"
     get "manage", to: "users#manage"  
   end
end

 root 'welcome#index'

end
